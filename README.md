# SPSP Loader

## Description
The SPSP Loader is part of the backend. It is the service that fetches the data on the SFTP server and then validates data before they are loaded in the SPSP database.

Sensitive data, like original sample identifiers, may be submitted to SPSP. SPSP is therefore hosted on a secure IT infrastructure (BioMedIT SENSA). As a consequence, the source code for the SPSP Loader may not be public, as it would provide information on server access and database model that could eventually make the platform vulnerable.

The source code for the SPSP Loader may however be shared upon request by trusted institutions. Please contact <spsp-support@sib.swiss>.

## Authors
Main developer: Daniel Walther

## License
For more information or any inquiries, please reach out to legal@sib.swiss.
